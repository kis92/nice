import java.io.*;

/**
 * Created by KiS on 22.10.2014.
 */
public class Nice {
    public static void main(String[] args) throws IOException {

        //read file
        String str = null;
        File fileInput = new File("C:\\nice\\input.txt");
        BufferedReader br = new BufferedReader (new InputStreamReader(new FileInputStream( fileInput )));
        String string2 = null;
        int[] input = new int[7];
        while ((string2 = br.readLine()) != null) {
            str = string2;
        }
        br.close();
        System.out.println( str );
        //read console
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String str = reader.readLine();
        for (int i = 0; i < input.length; i++) {
            String strTemp = String.valueOf(str.charAt(i));
            input[i] = Integer.parseInt(strTemp);
        }
        int summ = input[0] * 2 +
                input[1] * 7 + input[2] * 6 + input[3] * 5 +
                input[4] * 4 + input[5] * 3 + input[6] * 2;
        System.out.println(summ);
        int deliv = summ % 11;
        System.out.println(deliv);
        char ch = 'q';
        switch (deliv){
            case 0: ch = 'J';
                break;
            case 1: ch = 'A';
                break;
            case 2: ch = 'B';
                break;
            case 3: ch = 'C';
                break;
            case 4: ch = 'D';
                break;
            case 5: ch = 'E';
                break;
            case 6: ch = 'F';
                break;
            case 7: ch = 'G';
                break;
            case 8: ch = 'H';
                break;
            case 9: ch = 'I';
                break;
            case 10: ch = 'Z';
                break;
        }

        //out console
        System.out.println(str + ch);

        //out file
        File fileOut = new File("C:\\nice\\output.txt");
        FileWriter fileWriter = new FileWriter(fileOut);
        fileWriter.append(ch);
        fileWriter.flush();
    }
}
